package rest_api;

import java.util.Iterator;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

public class GameManager {

	public List<Game> getGames() {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		List games = session.createSQLQuery("Select gamemode_id, gamemode_name From gamemode_db").list();

		for (Iterator iter = games.iterator(); iter.hasNext();) {
			Word g = (Word) iter.next();
			System.out.println(g.toString());
		}
		session.getTransaction().commit();

		return games;
	}

}
