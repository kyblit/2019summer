package rest_api;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "gamemode_db")
public class Game implements java.io.Serializable{
	
	@Id
	@Column(name="gamemode_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="gamemode_name")
	private String name;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name = "gamemode_category_db",
			joinColumns = {@JoinColumn(name = "category_relation")},
			inverseJoinColumns = {@JoinColumn(name = "gamemode_relation")})
	private Set<Category> g_categories = new HashSet<>();
	
	public Game() {}
	
	public Game(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		String temp = "{\n" + 
						"\tid: " + id + "\n" + 
						"\tname: " + name + "\n" +
						"\tcategories: [";
		for(Category c : g_categories) {
			temp += c.toString() + "\n";
		}
		
		return temp + "\t]\n}";
	}
	
}
