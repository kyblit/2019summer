package rest_api;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="word_db")
public class Word implements java.io.Serializable{

	@Id
	@Column(name = "word_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "word")
	private String word;
	
	@Column(name = "image_path")
	private String picturePath;
	
	@Column(name = "contributer")
	private String contributer;
	
	@Column(name = "curated_satus")
	private int curated;
	//private String tmstp;
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
			name = "relation_db",
			joinColumns = {@JoinColumn(name = "word_id")},
			inverseJoinColumns = {@JoinColumn(name = "category_id")})
	private Set<Category> w_categories = new HashSet<>();
	
	public Word(int id, String word, String picturePath, String category, String contributer, String tmstp) {
		this.id = id;
		this.word = word;
		this.picturePath = picturePath;
		this.curated = 0;
		this.contributer = contributer;
	//	this.tmstp = tmstp;
	}
	
	public int getId() {
		return id;
	}
	
	public String getWord() {
		return word;
	}
	
	public String getPath() {
		return picturePath;
	}
	
	public int getCurated() {
		return curated;
	}
	
	public String getContributer() {
		return contributer;
	}
	
	@Override
	public String toString() {
		return "{\n" + 
				"\tid: " + id + "\n" + 
				"\tname: " + word + "\n" + 
				"\tpicture_path: " + picturePath + "\n" +
				"\tcurated_status: " + curated + "\n" +
				"\tcontributer: " + contributer + "\n" + 
				"}";
	}
}
