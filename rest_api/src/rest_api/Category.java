package rest_api;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "category_db")
public class Category {
	
	@Id
	@Column(name = "category_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="category_name")
	private String name;
	
	@Column(name = "curated_status")
	private int curated;
	
	@Column(name = "contributer")
	private String contributer;
	
	@ManyToMany(mappedBy = "g_categories")
	private Set<Game> games = new HashSet<>();
	
	@ManyToMany(mappedBy = "w_categories")
	private Set<Word> words = new HashSet<>();
	
	public Category(int categoryId, String category, int curated, String contributer) {
		this.id = categoryId;
		this.name = category;
		this.curated = curated;
		this.contributer = contributer;
	}
	
	public int getId() {
		return id;
	}
	
	public String getCategory() {
		return name;
	}
	
	public int getCuratedStatus() {
		return curated;
	}
	
	public String getContributer() {
		return contributer;
	}
	
	@Override
	public String toString() {
		String temp = "{\n" + 
						"\tcategory_id: " + id + "\n" + 
						"\tcategory_id: " + name + "\n" + 
						"\tcurated_status: " + curated + "\n" + 
						"\tcontributer: " + contributer + "\n" + 
						"\twords: [\n";
		for(Word w : words) {
			temp += w.toString() + "\n";
		}
		
		temp += "\t]\n}";
		
		return temp;
	}
}
