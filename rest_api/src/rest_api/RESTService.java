package rest_api;

import java.io.InputStream;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class RESTService {

	@GET
	@Path("/game")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGame() {
		return Response.status(200).entity("").build();
	}
	
	@GET
	@Path("/config")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfig(@QueryParam("Game") String game) {
		return Response.status(200).entity("").build();
	}
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getList(@QueryParam("listId") int listId) {
		return Response.status(200).entity("").build();
	}
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response putData() {
		return Response.status(200).entity("").build();
	}
	
	
	
	@GET
	@Path("/verify")
	@Produces(MediaType.TEXT_PLAIN)
	public Response verifyRESTService(InputStream incomingData) {
		String result = "Service Successfully started..";
 
		return Response.status(200).entity(result).build();
	}
	
}
